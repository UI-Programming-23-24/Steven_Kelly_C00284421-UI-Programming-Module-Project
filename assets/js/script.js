const canvas = document.getElementById("canvas")
const context = canvas.getContext("2d");

context.imageSmoothingEnabled = false;

let levelBackground = new Image();
levelBackground.src = "assets/img/egypt.jpg";

let playerImage = new Image();
playerImage.src = "assets/img/Green-16x18-spritesheet.png";

let enemyImage = new Image();
enemyImage.src = "assets/img/spikeEnemyLeft.png";

let portalImage = new Image();
portalImage.src = "assets/img/portal.png";

let rewardImage = new Image();
rewardImage.src = "assets/img/endGoal.png";

let weaponImage = new Image();
weaponImage.src = "assets/img/stone.png";

let collisionSound = new Audio("assets/audio/mixkit-squeak-notification-1017.wav");
console.log(collisionSound);

let portalSound = new Audio("assets/audio/494484__vic5555__portal-phase-jump.wav");
console.log(portalSound);

let weaponSound = new Audio("assets/audio/523769__matrixxx__retro-hit.wav");
console.log("Weapon colliion sound");

let floorLevel = 360;

let portalDrawn = true;
let rewardDrawn = false;

const backgroundWidth = 1100;
const backgroundHeight = 500;

let enemyScale = 0.85;
const intialHeight = 54;
const intialWidth = 48;
let enemyWidth = intialWidth * enemyScale;
let enemyHeight = intialHeight * enemyScale;
let enemyHealth = 1;
let enemyAlive = true;

const portalHeight = 120;
const portalWidth = 48;

const rewardWidth = 120;
const rewardHeight = 60;

const jumpHeight = 95;
let jumping = false;
let jumpCount = 0;
let jumpDirection = 1; // 1 for upward, -1 for downward

let weaponScale = 1;
const initialWeaponHeight = 15;
const initialWeaponWidth = 15;
let currentWeaponWidth = initialWeaponWidth * weaponScale;
let currentWeaponHeight = initialWeaponHeight * weaponScale;

// dimension variables & scaling for spritesheet
let playerScale = 3;
const width = 16;
const height = 18;
const widthScaled = playerScale * width;
const heightScaled = playerScale * height;

let player = new GameObject(playerImage, 1, floorLevel, width, height);
let background = new GameObject(levelBackground, 0, 0, backgroundWidth, backgroundHeight);
let enemy = new GameObject(enemyImage, 502, 364, enemyWidth, enemyHeight);
let portal = new GameObject(portalImage,(canvas.width - portalWidth), 290, portalWidth, portalHeight);
let reward = new GameObject(rewardImage, 800, 377, rewardWidth, rewardHeight);
let weapon = new GameObject(weaponImage, player.x, player.y, currentWeaponWidth, currentWeaponHeight);

let playerHealth = 5;
let playerOutside = false;
let enemyMoveRight = false;
let egyptLevel = true;
let ancientLevel = false;
let medievalLevel = false;
let enemyIsTurtle = true;
let enemyIsCenturion = false;
let enemyIsOrc = false;
let attacking = false;
let bulletCollided = false;
let playerFacingRight = true;

const walkLoop = [0, 1, 0, 2]; 
const frameLimit = 5;
const playerSpeed = 4; // allow sprite to move this many pixels per movement
let enemySpeed = 2;
let loopIndex = 0; // loop through array
let frameCount = 0; // control frames per second of animation
let direction = 0;
let knockback = 75;

// position and dimension information about the spritesheet
function GameObject(spritesheet, x, y, width, height)
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.direction = "None";
}



function GamerInput(input)
{
    this.action = input; // holds the current input as a string
}

let gamerInput = new GamerInput("None"); 


// player input stored as strings
function input(event)
 {
    console.log("Event type: " + event.type);
    
    if (event.type === "keydown") 
    {
        console.log("Key pressed: " + event.key);

        switch (event.key) 
        {
            case "ArrowLeft" :
                gamerInput = new GamerInput("Left");
                break;
            case "ArrowRight" :
                gamerInput = new GamerInput("Right");
                break;
            case "ArrowUp" :
                gamerInput = new GamerInput("Jump");
                break;
            case "Enter" :
                gamerInput = new GamerInput("attack");
                break;
            default:
                gamerInput = new GamerInput("None");
        }
    }
    else 
    {
        gamerInput = new GamerInput("None");
    }
}

function update() {
    checkBoundaryPlayer();
    checkBoundaryEnemy();
    collision();

    if (playerOutside == true)
    {
        console.log("boundary detected by Player");
    }

    if (gamerInput.action === "Left") 
    {
        console.log("Move Left");
        playerFacingRight = false;
        player.x -= playerSpeed;
        direction = 2;
    } 
    else if (gamerInput.action === "Right") 
    {
        console.log("Move Right");
        playerFacingRight = true;
        player.x += playerSpeed;
        direction = 3;
    }
    else if (gamerInput.action === "attack") 
    {
        console.log("Player Attacked");

        attacking = true;
        attack();
    }

    if (gamerInput.action === "Jump" && !jumping) 
    {
        console.log("Player Jump");
        jumping = true;
        jumpDirection = -1; // Start jumping upward
    }

    if (attacking == true)
    {
        if(playerFacingRight == true)
        {
            weapon.x += 5;
        }
        
        else
        {
            weapon.x -= 5;
        }
    }

    jump();  
    setEnemySize();
}

function setEnemySize()
{
    enemyHeight = intialHeight * enemyScale;
    enemyWidth = intialHeight * enemyScale;

    console.log("Enemy size set");
}

function setWeaponSize()
{
    currentWeaponHeight = initialWeaponHeight * weaponScale;
    currentWeaponWidth = initialWeaponWidth * weaponScale;

    weapon.width = currentWeaponWidth;
    weapon.height = currentWeaponHeight;

    console.log("weapon size set");
}

function attack() 
{
    attacking = true;

    if (playerFacingRight) 
    {
        weapon.x = player.x + widthScaled; // Set the weapon to the right of the player
    } 
    else 
    {
        weapon.x = player.x - weapon.width; // Set the weapon to the left of the player
    }

    weapon.y = player.y + (heightScaled - weapon.height) / 2; // Center the weapon vertically on the player

    weapon.x += 20; // Move the weapon forward

    // Rest of the attack function...
}

const gravity = 1; // Adjust the gravity value as needed

function jump() {
    if (jumping) 
    {
        if (jumpDirection === -1 && jumpCount < jumpHeight) {
            player.y -= jumpHeight / jumpHeight; // Adjust this value based on the desired jump height
            jumpCount++;
        }
         else 
         {
            player.y += gravity; // Simulate gravity to make the player fall

            if (player.y >= floorLevel) {
                // Adjust this value based on the ground level
                jumping = false;
                jumpCount = 0;
                player.y = floorLevel; // Set player y position to ground level
            }
        }
    }
}






function enemyMovement()
 {
    if(enemyMoveRight == false)
    {
        enemy.x -= enemySpeed;
    }
    else if (enemyMoveRight == true)
    {
        enemy.x += enemySpeed;
    }      
 }
  
 



// animate spritesheet 
function animate()
{
    if (gamerInput.action != "None") // if correct inputs are stored in gamerInput.action
    {
        frameCount++;
        if (frameCount >= frameLimit) 
        {
            frameCount = 0;
            loopIndex++;
            if (loopIndex >= walkLoop.length) // restart if the end of the walk cycle is reached.
            {
                loopIndex = 0;
            }
        }
    }
    else 
    {
        loopIndex = 0; // no animation if no input
    }
    drawFrame(player.spritesheet, walkLoop[loopIndex], direction, player.x, player.y); // draw the frame whether it's updated or not
}

// pass required dimensions of the image into the frame
function drawFrame(image, frameX, frameY, canvasX, canvasY)
{
    context.drawImage(image, frameX * width, frameY * height, width, height, canvasX, canvasY, widthScaled, heightScaled);
}

function draw()
{
    context.clearRect(0, 0, canvas.width, canvas.height);
    console.log("Draw");

    context.drawImage(background.spritesheet, 
      background.x,
      background.y,
      background.width,
      background.height);

      if(enemyAlive == true)
      {
        context.drawImage(enemy.spritesheet, 
        enemy.x,
        enemy.y,
        enemyWidth,
        enemyHeight);
      }
      

      if(portalDrawn == true)
      {
        context.drawImage(portal.spritesheet, 
            portal.x,
            portal.y,
            portal.width,
            portal.height);
      }

      if(rewardDrawn == true)
      {
        context.drawImage(reward.spritesheet, 
            reward.x,
            reward.y,
            reward.width,
            reward.height);
      }

        if(attacking == true)
        {
            context.drawImage(weapon.spritesheet, 
            weapon.x,
            weapon.y,
            weapon.width,
            weapon.height);
        }
      
    animate();
}

function collision() 
{

    if(enemyAlive == true)
    {
        if (player.x + widthScaled >= enemy.x &&
            player.x <= enemy.x + enemyWidth &&
            player.y + heightScaled >= enemy.y &&
            player.y <= enemy.y + enemyHeight)
         
            {
                collisionSound.play();
    
                if(enemyMoveRight == true)
                {
                    player.x += knockback;
                }
                else
                {
                    player.x -= knockback;
                }
    
                
                console.log("collision detected between player and enemy")
            }

            if (weapon.x + widthScaled >= enemy.x &&
                weapon.x <= enemy.x + enemyWidth &&
                weapon.y + heightScaled >= enemy.y &&
                weapon.y <= enemy.y + enemyHeight)
             
                {
                    weaponSound.play();
                    if(attacking == true)
                    {
                        enemyHealth -= 1;
                    }

                    attacking = false;
        
                    if(enemyHealth == 0)
                    {
                        enemyAlive = false;
                    }

                    console.log("attack collided with enemy")
                }
    }
    

    if (player.x <= portal.x + portal.width && 
        player.x + player.width >= portal.x && 
        player.y <= portal.y + portal.height &&
        player.y + player.height >= portal.y && portalDrawn == true) 
        {
            changeLevel();

            console.log("player time travelled")
        }

        if (player.x <= reward.x + reward.width && 
            player.x + player.width >= reward.x && 
            player.y <= reward.y + reward.height &&
            player.y + player.height >= reward.y && rewardDrawn == true) 
            {
                rewardDrawn = false;
                console.log("player collected final reward")
            }
 }

function changeLevel()
{
    enemyAlive = true;

    if(egyptLevel == true)
    {
        ancientLevel = true;
        enemyIsTurtle = false;
        enemyIsCenturion = true;

        enemyHealth = 4;

        enemyScale = 1.2;
        setEnemySize();

        enemy.y -= 15;

        player.x = 1;
        player.y -= 70;

        floorLevel -= 70;

        portal.y -= 70;
        portalSound.play();

        enemy.y -= 70;

        weaponScale = 5;
        setWeaponSize();

        if(enemyMoveRight == true)
        {
            enemyImage.src = "assets/img/centurionRight.png";
            weaponImage.src = "assets/img/spearRight.png";
        }
        else if(enemyMoveRight == false)
        {
            enemyImage.src = "assets/img/centurionLeft.png";
            weaponImage.src = "assets/img/spearLeft.png";
        }


        levelBackground.src = "assets/img/ancient.jpg";

        egyptLevel = false;


    }
    else if (ancientLevel == true) {
        enemySpeed = 0;

        enemyScale = 5;
        setEnemySize();

        enemyHealth = 10;

        enemyHeight = 108;
        medievalLevel = true;
        enemyIsCenturion = false;
        enemyIsOrc = true;

        weaponScale = 3;
        setWeaponSize();
        
        player.x = 1;
        enemy.x = 400;

        player.y += 90;
        enemy.y -= 100;

        if(enemyMoveRight == true) 
        {
            enemyImage.src = "assets/img/orcEnemyRight.png"
        } 
        else if(enemyMoveRight == false && medievalLevel) {
            enemyImage.src = "assets/img/orcEnemyLeft.png"
        }

        

        floorLevel += 90;

        portalSound.play();

        levelBackground.src = "assets/img/medieval.jpg";
        weaponImage.src = "assets/img/fireSpell.png";
        ancientLevel = false;
        portalDrawn = false;
        rewardDrawn = true;
    }


}

function checkBoundaryPlayer(){
    if (player.x < 0)
        {
        player.x = 0;
        playerOutside = true;
        }
    else if (player.x >= canvas.width - widthScaled)
        {
            player.x = canvas.width - widthScaled;
            playerOutside = true;
        }
    else if (player.x >= canvas.width - (widthScaled + 120) && medievalLevel == true)
        {
            player.x = canvas.width - (widthScaled + 120);
            playerOutside = true;
        }
    else if (weapon.x < 0)
        {
            attacking = false;
        }
    else if (weapon.x >= canvas.width - widthScaled)
        {
            attacking = false;
        } 
    else 
        {
        playerOutside = false;
        }
    }

    function checkBoundaryEnemy() {
        if (enemy.x <= 0) 
        {
            if(enemyIsTurtle == true)
            {
                enemyImage.src = "assets/img/spikeEnemyRight.png";
            }
            else if(enemyIsCenturion == true)
            {
                enemyImage.src = "assets/img/centurionRight.png";
            }
            else if(enemyIsOrc == true)
            {
                enemyImage.src = "assets/img/orcEnemyRight.png";
            }

          enemyMoveRight = true;
        } 
        else if (enemy.x >= (785 - enemy.width))
        {
          enemyMoveRight = false;
          if(enemyIsTurtle == true)
            {
                enemyImage.src = "assets/img/spikeEnemyLeft.png";
            }
            else if(enemyIsCenturion == true)
            {
                enemyImage.src = "assets/img/centurionLeft.png";
            }
            else if(enemyIsOrc == true)
            {
                enemyImage.src = "assets/img/orcEnemyLeft.png";
            }
        }
      }
    
function gameloop() 
{
    update();
    enemyMovement();
    jump(); // Move the jump function call here to handle jumping in every frame
    draw();
    window.requestAnimationFrame(gameloop);
}



window.requestAnimationFrame(gameloop);

window.addEventListener('keydown', input);
window.addEventListener('keyup', input);